import firebase from 'react-native-firebase'
import type { Notifications,NotificationOpen} from 'react-native-firebase'

class FCMService {
    register = (onRegister, onNotification, onOpenNotification) =>{
        this.checkPermission(onRegister)
        this.createNotificationListeners(onRegister,onNotification, onOpenNotification)    
    }

    checkPermission = (onRegister) =>{
            firebase.messaging().hasPermission()
            .then(enabled =>{
                if (enabled){
                    // User has permission
                    this.getToken(onRegister)
                } else {
                    //User lacks permission
                    this.requestPermission(onRegister)
                }
            }).catch(error =>{
                console.log("Permission rejected", error)
            })
    }

    getToken = (onRegister) =>{
            firebase.messaging().getToken()
             .then(fcmToken =>{
                if (fcmToken){
                    onRegister(fcmToken)
                } else {
                    console.log("User Doesn't have device token")
                }
            })
                .catch(error=>{
                    console.log("token rejected", error)
                })
    }

    requestPermission = (onRegister) =>{
        firebase.messaging().requestPermission()
        .then(()=>{
            this.getToken(onRegister)
        }).catch(error =>{
            console.log("Request Permission rejected",error)
        })
    }

    deleteToken =()=>{
        firebase.messaging().deleteToken()
        .catch(error=>{
            console.log("Delete token error", error)
        })
    }


    createNotificationListeners = (onRegister,onNotification,onOpenNotification) =>{
        //Triggered when notification is recieved
        this.notificationListener = firebase.notifications().onNotification((notification: Notification)=>{
            onNotification(notification)
        })
        //if your app in back u can listen to notifications is open/tapped clicked
        this.notificationOpenedListener = firebase.notifications()
        .onNotificationOpened((notificationOpen: NotificationOpen)=>{
            onOpenNotification(notification)
        })
        // if your app was closed you can check if it was opened by a notification
        //being clicked //tape /open
        firebase.notifications().getInitialNotification()
        .then({
            if(notificationOpen){
                const notification: Notification = notificationOpen.notification
                onOpenNotification(notification)
            }
        })

        //Triggered for data only payload in the background
        this.messageListener = firebase.messaging().onMessage((message)=>{
            onNotification(message)
        })

        // Triggered  when we have new token
        this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken=>{
            console.log("New Token Refresh:",fcmToken )
            onRegister(fcmToken)
        })


    }
    unRegister = () =>{
        this.notificationListener()
        this.notificationOpenedListener()
        this.messageListener()
        this.onTokenRefreshListener()
    }

    buildChannel = (obj) =>{
        return new firebase.notification.Android.Channel(
            obj.channelID, obj. channelName,
            firebase.notifications.Android.Importance.High
        )
        .setDescription(obj.channelDes)
    }

    buildNotification = (obj) =>{
        //for android
        firebase.notification().android.createChannel(obj.channel)
        // for android and ios
        return new firebase.notifications.Notification()
        .setSound(obj.sound)
        .setNotificatio(obj.dataId)
        .setTitle(obj.title)
        .setContent(obj.content)
        .setData(obj.data)
        //for android
        .android.setChannelId(obj.channel.channelID)
        .android.setLargeIcon(obj.largeIcon) // create this icon in android studio (app/res/mipmap)
        .android.setSmallIcon(obj.smallIcon) // create this icon in android studio (app/res/drawable)
        .android.setColor(obj.colorBgIcon)
        .android.setPriority(firebase.notifications.Android.priority.High)
        .android.setVibrate(obj.vibrate)
        //.android.setAutoCancel(true)// auto cancel after recieve notification

    }

    scheduleNotification = (notification, days ,minutes) =>{
        const date = new Date()
        if (days){
            date.setDate(date.getDate() + days)
        }
        if (minutes){
            date.setMinutes(date.getMinutes() + minutes)
        }
        firebase.notifications().scheduleNotification(notification, {fireDate:date.getTime()})
    }

    displayNotification = (notification) =>{
        firebase.notifications().displayNotification(notification)
        .catch(error=> console.log("Display Notification error:", error))
    }

    removeDeliveredNotification = (notification) =>{
        firebase.notifications()
            .removeDeliveredNotification(notification.notificationId)
    }
}

export const fcmService = new FCMService()