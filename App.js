
import React, {Component} from 'react'
import {StyleSheet,View, Text,} from 'react-native';
import {fcmService} from './src/FCMService'


export default class App extends  Component {
  constructor(props) {
    super(props)
    this.fcmNotification = fcmService
  }

  componentDidMount (){
    this.fcmNotification = fcmService
    this.fcmNotification.register(this.onRegister,this.onNotification,
    this.onOpenNotification)
  }

  onRegister(token){
    console.log("[NotificationFCM] onRegister:", token)
  }

  onNotification(notify){
    console.log("[NotificationFCM] onNotification:", notify)
  }

  onOpenNotification(notify){
    console.log("[NotificationFCM] onNotification: ", notify)
  }

  render () {
    let {container} = styles
    return(
      <View style={container}>
      <Text>Sample RecatNative Firebase</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container:{
    flex: 1,
    alignItems: 'center',
    justifyContent:'center'
  },
});

